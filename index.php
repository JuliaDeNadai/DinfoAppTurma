<?php
require './vendor/autoload.php';

$app = (new DinfoApp\App())->get();
$app->run();
