<?php
namespace DinfoApp\Tests;
require '../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use DinfoApp\App;

class CadCursoTest extends TestCase
{
    protected $app;
    protected $id;

    public function setUp()
    {
        $this->app = (new App())->get();
        $this->id=0;
    }

    public function testGetCursos()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/curso',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }

    public function testPostCurso()
    {
        $dados = array('nome'=>'Curso de Testes','sigla'=>'CT','idDepartamento'=>1,'idChefe'=>1);
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => 'api/v1/curso',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->id = $result["data"]["id"];
        return $this->id;
    }


    /**
     * @depends testPostCurso
     */
    public function testPutCurso($id)
    {
        $dados = array('nome'=>'Test Course','sigla'=>'TC','idDepartamento'=>1,'idChefe'=>1);
        $env = Environment::mock([
            'REQUEST_METHOD' => 'PUT',
            'REQUEST_URI'    => 'api/v1/curso/'.$id,
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"],"sucesso");
    }

    /**
     * @depends testPostCurso
     */
    public function testDeleteUsuario($id)
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'DELETE',
            'REQUEST_URI'    => 'api/v1/curso/'.$id,
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody([]);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }
}
