<?php
namespace DinfoApp\Tests;
require '../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use DinfoApp\App;
class UsuarioTest extends TestCase
{
    protected $app;
    protected $id;

    public function setUp()
    {
        $this->app = (new App())->get();
        $this->id=0;
    }

    public function testGet()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $this->assertSame((string)$response->getBody(), '{"message":"Page not found"}');
    }

    public function testGetUsuarios()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }

    public function testGetUsuario()
    {
        $jsonEsperado='{
    "code": 200,
    "status": "sucesso",
    "data": {
        "idUsuario": "1",
        "nome": "José Alberto Matioli",
        "email": "matioli@unicamp.br",
        "senha": "014fc2ce795c62b2e063d867d4df2336",
        "situacao": "0",
        "token": null,
        "criado_em": "2017-09-20 13:02:29"
    }
}';

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario/1',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
        $this->assertJsonStringEqualsJsonString($jsonEsperado, json_encode($result));

    }
    public function testPostUsuario()
    {
        $dados = array('nome'=>'UsuarioTest Test OITO','email'=>'ut8@ut.com','senha'=>'teste');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'POST',
            'REQUEST_URI'    => 'api/v1/usuario',
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->id = $result["data"]["id"];
        return $this->id;

    }

    /**
     * @depends testPostUsuario
     */
    public function testPutUsuario($id)
    {
        $dados = array('nome'=>'UsuarioTest Test 88888888888888','email'=>'ut88@ut.com','senha'=>'teste');
        $env = Environment::mock([
            'REQUEST_METHOD' => 'PUT',
            'REQUEST_URI'    => 'api/v1/usuario/'.$id,
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"],"sucesso");
    }

    /**
     * @depends testPostUsuario
     */
    public function testDeleteUsuario($id)
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'DELETE',
            'REQUEST_URI'    => 'api/v1/usuario/'.$id,
            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
        ]);
        $req = Request::createFromEnvironment($env)->withParsedBody([]);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }
}