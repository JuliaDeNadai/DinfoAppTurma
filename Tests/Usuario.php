<<<<<<< HEAD
<?php
use PHPUnit\Framework\TestCase;
use DinfoApp\App;
use Slim\Http\Environment;
use Slim\Http\Request;

class Usuario extends TestCase
{
    protected $app;
    protected $id;

    public function setUp()
    {
        $this->app = (new App())->get();
    }

    public function testGet()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $this->assertSame((string)$response->getBody(), '{"message":"Page not found"}');
    }

    public function testGetUsuarios()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }

    public function testGetUsuario()
    {
        $jsonEsperado='{
    "code": 200,
    "status": "sucesso",
    "data": {
        "id": "62",
        "nome": "Usuario Test OITO",
        "documento": "888888888",
        "email": "ut8@ut.com",
        "senha": "698dc19d489c4e4db73e28a713eab07b",
        "status": "0",
        "criado_em": "2017-09-12 11:02:24",
        "nivel": "0",
        "uuid": null,
        "token": null
    }
}';

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario/62',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
        $this->assertJsonStringEqualsJsonString($jsonEsperado, json_encode($result));

    }
//    public function testPostUsuario()
//    {
//        $dados = array('nome'=>'Usuario Test OITO','email'=>'ut8@ut.com','documento'=>'888888888','senha'=>'teste');
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'POST',
//            'REQUEST_URI'    => 'api/v1/usuario',
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->id = $result["data"]["id"];
//    }
//
//    public function testPutUsuario()
//    {
//        $dados = array('nome'=>'Usuario Test 88888888888888','email'=>'ut88@ut.com','documento'=>'81818181','senha'=>'teste');
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'PUT',
//            'REQUEST_URI'    => 'api/v1/usuario/'.$this->id,
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->assertSame($result["status"],"sucesso");
//    }
//    public function testDeleteUsuario()
//    {
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'DELETE',
//            'REQUEST_URI'    => 'api/v1/usuario/'.$this->id,
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody([]);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->assertSame($result["status"], "sucesso");
//    }
=======
<?php
use PHPUnit\Framework\TestCase;
use DinfoApp\App;
use Slim\Http\Environment;
use Slim\Http\Request;

class Usuario extends TestCase
{
    protected $app;
    protected $id;

    public function setUp()
    {
        $this->app = (new App())->get();
    }

    public function testGet()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => '/',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 404);
        $this->assertSame((string)$response->getBody(), '{"message":"Page not found"}');
    }

    public function testGetUsuarios()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }

    public function testGetUsuario()
    {
        $jsonEsperado='{
    "code": 200,
    "status": "sucesso",
    "data": {
        "id": "62",
        "nome": "Usuario Test OITO",
        "documento": "888888888",
        "email": "ut8@ut.com",
        "senha": "698dc19d489c4e4db73e28a713eab07b",
        "status": "0",
        "criado_em": "2017-09-12 11:02:24",
        "nivel": "0",
        "uuid": null,
        "token": null
    }
}';

        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/usuario/62',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
        $this->assertJsonStringEqualsJsonString($jsonEsperado, json_encode($result));

    }
//    public function testPostUsuario()
//    {
//        $dados = array('nome'=>'Usuario Test OITO','email'=>'ut8@ut.com','documento'=>'888888888','senha'=>'teste');
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'POST',
//            'REQUEST_URI'    => 'api/v1/usuario',
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->id = $result["data"]["id"];
//    }
//
//    public function testPutUsuario()
//    {
//        $dados = array('nome'=>'Usuario Test 88888888888888','email'=>'ut88@ut.com','documento'=>'81818181','senha'=>'teste');
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'PUT',
//            'REQUEST_URI'    => 'api/v1/usuario/'.$this->id,
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody($dados);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->assertSame($result["status"],"sucesso");
//    }
//    public function testDeleteUsuario()
//    {
//        $env = Environment::mock([
//            'REQUEST_METHOD' => 'DELETE',
//            'REQUEST_URI'    => 'api/v1/usuario/'.$this->id,
//            'CONTENT_TYPE'   => 'application/x-www-form-urlencoded',
//        ]);
//        $req = Request::createFromEnvironment($env)->withParsedBody([]);
//        $this->app->getContainer()['request'] = $req;
//        $response = $this->app->run(true);
//        $this->assertSame($response->getStatusCode(), 200);
//        $result = json_decode($response->getBody(), true);
//        $this->assertSame($result["status"], "sucesso");
//    }
>>>>>>> bcb6f7b17579cbf1e30dd782153a6d92b07276a1
}