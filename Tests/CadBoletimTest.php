<?php

namespace DinfoApp\Tests;
require '../vendor/autoload.php';

use PHPUnit\Framework\TestCase;
use Slim\Http\Environment;
use Slim\Http\Request;
use DinfoApp\App;

class CadBoletimTest extends TestCase
{
    protected $app;
    protected $id;

    public function setUp()
    {
        $this->app = (new App())->get();
    }

    public function testGetCursos()
    {
        $env = Environment::mock([
            'REQUEST_METHOD' => 'GET',
            'REQUEST_URI'    => 'api/v1/boletim/12345',
        ]);
        $req = Request::createFromEnvironment($env);
        $this->app->getContainer()['request'] = $req;
        $response = $this->app->run(true);
        $this->assertSame($response->getStatusCode(), 200);
        $result = json_decode($response->getBody(), true);
        $this->assertSame($result["status"], "sucesso");
    }
}
