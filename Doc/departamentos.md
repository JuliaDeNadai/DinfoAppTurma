<<<<<<< HEAD
# DINFO Api

# Group Cadastros

## Departamentos [/departamentos]
Para obter uma listagem de todos os departamentos cadastrados.
### GET 

+ Response 200 (application/json)
    + Attributes
        - departamentos (array[Departamento Full])

## Departamento [/departamentos/{id}]
Operações a serem realizadas sobre um departamento específico.

+ Parameters
  + id: '1' - Identificados único de cada departamento

### GET 

+ Response 200 (application/json)
    + Attributes
        departamentos (Departamento Full)

### POST

+ Request (application/json)
    + Attributes
        - departamentos (Departamento Create)

+ Response 200 (application/json)
    + Attributes
        - departamentos (Departamento Full)

### PUT

+ Request (application/json)
    + Attributes
      - departamentos (Departamento Full)

+ Response 200 (application/json)
    + Attributes
        - departamentos (Departamento Full)
        
### DELETE
+ Response 204 
        
        
# Data Structures

## Departamento Create (object)
    - sigla: 'DINFO' (string, required)
    - nome: 'Departamento de Informática' (string, required)        
    - chefe: 'Profa. Rosana' (string, requires)
    

## Departamento Full (object)
    - id: '1' (number, required) 
    - Inclue Departamento Create
=======
# DINFO Api

# Group Cadastros

## Departamentos [/departamentos]
Para obter uma listagem de todos os departamentos cadastrados.
### GET 

+ Response 200 (application/json)
    + Attributes
        - departamentos (array[Departamento Full])

## Departamento [/departamentos/{id}]
Operações a serem realizadas sobre um departamento específico.

+ Parameters
  + id: '1' - Identificados único de cada departamento

### GET 

+ Response 200 (application/json)
    + Attributes
        departamentos (Departamento Full)

### POST

+ Request (application/json)
    + Attributes
        - departamentos (Departamento Create)

+ Response 200 (application/json)
    + Attributes
        - departamentos (Departamento Full)

### PUT

+ Request (application/json)
    + Attributes
      - departamentos (Departamento Full)

+ Response 200 (application/json)
    + Attributes
        - departamentos (Departamento Full)
        
### DELETE
+ Response 204 
        
        
# Data Structures

## Departamento Create (object)
    - sigla: 'DINFO' (string, required)
    - nome: 'Departamento de Informática' (string, required)        
    - chefe: 'Profa. Rosana' (string, requires)
    

## Departamento Full (object)
    - id: '1' (number, required) 
    - Inclue Departamento Create
>>>>>>> bcb6f7b17579cbf1e30dd782153a6d92b07276a1
    - created_at: `2015-01-07T14:03:43Z` (string, required) - ISO8601 data e hora do momento que o departamento foi criado.