<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 05/09/2017
 * Time: 09:51
 */

namespace DinfoApp\Util;

class Util
{
    public static function getRandom($length)
    {
        $char = array_merge(range(0, 9), range('A', 'Z'), range('a', 'z'));
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= $char[mt_rand(0, count($char) - 1)];
        }
        return $code;
    }

}