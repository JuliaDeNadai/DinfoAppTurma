<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 05/09/2017
 * Time: 09:33
 */

namespace DinfoApp\Login;

use DinfoApp\Database\PDOConnection;
use DinfoApp\Util\Util;
use \PDO;

class Login
{


    public function login($email, $senha)
    {
        $conn = PDOConnection::instance()->getConnection();

        $response = array();
        $encPwd = md5($senha); // You can use more secure password encryption method
        $sql = "SELECT idUsuario, nome, email, situacao FROM usuario WHERE email = :email AND senha = :senha";
        $res = $conn->prepare($sql);
        $res->bindParam(':email', $email);
        $res->bindParam(':senha', $encPwd);
        $res->execute();
        $record = $res->fetch(PDO::FETCH_ASSOC);
        if (!empty($record)) {
            if($record['situacao']==1) {
                $token = Util::getRandom(50);
                $updateSql = "UPDATE usuario SET token = :token WHERE idUsuario = :idUsuario";
                $updateRes = $conn->prepare($updateSql);
                $updateRes->bindParam(':token', $token);
                $updateRes->bindParam(':idUsuario', $record['idUsuario']);
                $updateRes->execute();

                $response['code'] = 200;
                $response['status'] = 'sucesso';
                $response['token'] = $token;
                $response['data'] = array('nome' => $record['nome'], 'email' => $record['email']);
            }else{
                $response['code'] = 401;
                $response['status'] = 'Falha: conta inativa';
            }

        } else {
            $response['code'] = 401;
            $response['status'] = 'Falha: conta não cadastrada';
        }

        return $response;
    }

    public function loginWithToken($token)
    {
        $conn = PDOConnection::instance()->getConnection();
        $response = array();
        $sql = "SELECT idUsuario, nome, email, status FROM usuario WHERE token = :token";
        $res = $conn->prepare($sql);
        $res->bindParam(':token', $token);
        $res->execute();
        $record = $res->fetch(PDO::FETCH_ASSOC);
        if (!empty($record)) {
            if($record['situacao']==1) {
                $token = Util::getRandom(50);
                $updateSql = "UPDATE usuario SET token = :token WHERE idUsuario = :idUsuario";
                $updateRes = $conn->prepare($updateSql);
                $updateRes->bindParam(':token', $token);
                $updateRes->bindParam(':idUsuario', $record['idUsuario']);
                $updateRes->execute();

                $response['code'] = 200;
                $response['status'] = 'sucesso';
                $response['token'] = $token;
                $response['data'] = array('nome' => $record['nome'], 'email' => $record['email']);
            }else{
                $response['code'] = 401;
                $response['status'] = 'Falha: conta inativa';
            }

        } else {
            $response['code'] = 401;
            $response['status'] = 'Falha: token inválido';
        }
        return $response;
    }

    public function logout($token)
    {
        $conn = PDOConnection::instance()->getConnection();
        $key = $this->verifyToken($token);
        if (!empty($key)) {
            $updateSql = "UPDATE usuario SET token = null WHERE token = :token";
            $updateRes = $conn->prepare($updateSql);
            $updateRes->bindParam(':token', $token);
            $updateRes->execute();

            return array('status' => 'sucesso', 'message' => 'Volte sempre!');
        } else {
            return array('status' => 'falha', 'message' => 'Token inválido!');
        }
    }

    private function verifyToken($token)
    {
        $conn = PDOConnection::instance()->getConnection();
        $sql = "SELECT idUsuario FROM usuario WHERE token = :token";
        $res = $conn->prepare($sql);
        $res->bindParam(':token', $token);
        $res->execute();
        $record = $res->fetch(PDO::FETCH_ASSOC);
        if (!empty($record)) {
            return $record['idUsuario'];
        } else {
            return false;
        }
    }

}