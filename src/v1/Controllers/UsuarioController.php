<?php
namespace DinfoApp\v1\Controllers;

use DinfoApp\v1\Models\Usuario;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

class UsuarioController
{
    /**
     * Container Class
     * @var [object]
     */
    private $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    function listaUsuarios(Request $request, Response $response, $args)
    {
        $usuarios = new Usuario();
        return $response->withJson($usuarios->getAll(), 200);
    }

    function listaUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $id = $args['id'];
        $usuarios = new Usuario();
        $resposta = $usuarios->get($id);
        return $response->withJson($resposta, $resposta['code']);
    }


    function criaUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $email = $request->getParam('email');
        $senha = $request->getParam('senha');
        $nome = $request->getParam('nome');
        $doc = $request->getParam('documento');
        if (!empty($email) && /* salva no banco */
            !empty($nome) &&
            !empty($senha)
        ) {
            $usuarios = new Usuario();
            $resposta = $usuarios->create($nome, $email, $doc, $senha);
        } else {
            $resposta['code'] = 401;
            $resposta['status'] = 'falha';
            $resposta['message'] = 'Por favor, informe os dados necessários!';
        }

        return $response->withJson($resposta, $resposta['code']);
    }

    function atualizaUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $id = $args['id'];
        $email = $request->getParam('email');
        $nome = $request->getParam('nome');
        if (!empty($email) &&
            !empty($nome) &&
            !empty($id)
        ) {
            $usuarios = new Usuario();
            $resposta = $usuarios->update($id, $nome, $email);
        } else {
            $resposta['code'] = 401;
            $resposta['status'] = 'falha';
            $resposta['message'] = 'Por favor, informe os dados necessários!';
        }

        return $response->withJson($resposta, $resposta['code']);
    }

    function ativaUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $id = $args['id'];
        $situacao = $request->getParam('situacao');

        if (strlen($situacao) && !empty($id) ) {
            $usuarios = new Usuario();
            $resposta = $usuarios->ativa_desativa($id,$situacao);
        } else {
            $resposta['code'] = 401;
            $resposta['status'] = 'falha';
            $resposta['message'] = 'Por favor, informe os dados necessários!';
        }

        return $response->withJson($resposta, $resposta['code']);
    }

    function excluiUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $id = $args['id'];
        if (!empty($id)) {
            $usuarios = new Usuario();
            $resposta = $usuarios->delete($id);
        } else {
            $resposta['code'] = 401;
            $resposta['status'] = 'falha';
            $resposta['message'] = 'Por favor, informe os dados necessários!';
        }

        return $response->withJson($resposta, $resposta['code']);
    }
    function zapUsuario(Request $request, Response $response, $args)
    {
        $resposta = array();
        $usuarios = new Usuario();
        $resposta = $usuarios->zap();

        return $response->withJson($resposta, $resposta['code']);
    }

}