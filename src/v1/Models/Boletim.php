<?php
/**
 * Created by PhpStorm.
 * User: Matioli
 * Date: 20/09/2017
 * Time: 11:03
 */

namespace DinfoApp\v1\Models;

class Boletim
{

    public function getAll()
    {
        $resposta = array(
            "code"=> 200,
            "status"=> "sucesso",
            "ra"=> "12345",
            "data"=> array(
                array(
                    "disciplina"=> "Desenvolvimento de Aplicações WEB",
                    "media"=> 5.96,
                    "faltas"=> 1
                ),
                array(
                    "disciplina"=> "Desenvolvimento para Dispositivos Moveis",
                    "media"=> 6,
                    "faltas"=> 2
                ),
                array(
                    "disciplina"=> "Linguagem de Programação Multiplataforma",
                    "media"=> 7.1,
                    "faltas"=> 3
                ),
                array(
                    "disciplina"=> "Desenvolvimento de Projetos de Software",
                    "media"=> 7.5,
                    "faltas"=> 4
                ),
            )
        );
        return $resposta;
    }

}