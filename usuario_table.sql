<<<<<<< HEAD
-- --------------------------------------------------------
-- Servidor:                     143.106.241.1
-- Versão do servidor:           5.5.53-0+deb8u1 - (Debian)
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela matioli.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `documento` varchar(20) DEFAULT NULL COMMENT 'Se Aluno = RA, Se Servidor = Matricula',
  `email` varchar(100) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Inativo\\n1 - Ativo',
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nivel` tinyint(4) DEFAULT '0',
  `uuid` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `documento` (`documento`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela matioli.usuarios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `documento`, `email`, `senha`, `status`, `criado_em`, `nivel`, `uuid`, `token`) VALUES
	(15, 'J. A. Matioli', '862034', 'matioli@unicamp.br', '014fc2ce795c62b2e063d867d4df2336', 1, '2017-06-06 00:24:11', 0, '5936205beeea93.75197849', 'mpfNmhbyd7VHEPza05wqk4z20aHBstlqhWVj17yDc0jvQdU5tI'),
	(20, 'Maria Xikinha', '123456', 'maria@xika.com', 'c33367701511b4f6020ec61ded352059', 1, '2017-06-06 01:47:18', 0, '593633d69270c2.50718959', NULL),
	(21, 'Mane', '123456789', 'mane@mn.com', 'e8d95a51f3af4a3b134bf6bb680a213a', 1, '2017-09-01 22:47:22', 0, NULL, NULL),
	(23, 'NOME', 'DOCUMENTO', 'EMAIL', '85ee0fe4f155a9af2657d85054ad63ca', 0, '2017-09-02 12:54:20', 0, NULL, NULL),
	(27, 'Manuelito', '12933219', 'm@m.com', 'e8d95a51f3af4a3b134bf6bb680a213a', 0, '2017-09-02 13:01:25', 0, NULL, NULL),
	(31, 'Felizbino', '0987654321', 'flz@xis.com', 'afe0b02b35aeae24ae55d34617857428', 1, '2017-09-05 15:59:22', 0, NULL, NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
=======
-- --------------------------------------------------------
-- Servidor:                     143.106.241.1
-- Versão do servidor:           5.5.53-0+deb8u1 - (Debian)
-- OS do Servidor:               debian-linux-gnu
-- HeidiSQL Versão:              9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Copiando estrutura para tabela matioli.usuarios
CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `documento` varchar(20) DEFAULT NULL COMMENT 'Se Aluno = RA, Se Servidor = Matricula',
  `email` varchar(100) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Inativo\\n1 - Ativo',
  `criado_em` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nivel` tinyint(4) DEFAULT '0',
  `uuid` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  UNIQUE KEY `documento` (`documento`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela matioli.usuarios: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` (`id`, `nome`, `documento`, `email`, `senha`, `status`, `criado_em`, `nivel`, `uuid`, `token`) VALUES
	(15, 'J. A. Matioli', '862034', 'matioli@unicamp.br', '014fc2ce795c62b2e063d867d4df2336', 1, '2017-06-06 00:24:11', 0, '5936205beeea93.75197849', 'mpfNmhbyd7VHEPza05wqk4z20aHBstlqhWVj17yDc0jvQdU5tI'),
	(20, 'Maria Xikinha', '123456', 'maria@xika.com', 'c33367701511b4f6020ec61ded352059', 1, '2017-06-06 01:47:18', 0, '593633d69270c2.50718959', NULL),
	(21, 'Mane', '123456789', 'mane@mn.com', 'e8d95a51f3af4a3b134bf6bb680a213a', 1, '2017-09-01 22:47:22', 0, NULL, NULL),
	(23, 'NOME', 'DOCUMENTO', 'EMAIL', '85ee0fe4f155a9af2657d85054ad63ca', 0, '2017-09-02 12:54:20', 0, NULL, NULL),
	(27, 'Manuelito', '12933219', 'm@m.com', 'e8d95a51f3af4a3b134bf6bb680a213a', 0, '2017-09-02 13:01:25', 0, NULL, NULL),
	(31, 'Felizbino', '0987654321', 'flz@xis.com', 'afe0b02b35aeae24ae55d34617857428', 1, '2017-09-05 15:59:22', 0, NULL, NULL);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
>>>>>>> bcb6f7b17579cbf1e30dd782153a6d92b07276a1
